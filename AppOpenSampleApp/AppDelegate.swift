//
//  AppDelegate.swift
//  AppOpenSampleApp
//
//  Created by Mohamed Matloub  on 30.06.21.
//

import UIKit
import AATKit
import AppTrackingTransparency

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    lazy var appOpenPlacement = AATSDK.createAppOpenAdPlacement(placementName: "placement")
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       
        let configuration = AATConfiguration()

        // !IMPORTANT! this line for this demo purpose only and shouldn't be used in live apps
        configuration.testModeAccountId = 1995

        AATSDK.initAATKit(with: configuration)
        AATSDK.setLogLevel(logLevel: .debug)
        
        // Start from iOS 14.5, you need to request Tracking Autherization in order to recieve personalized Ads
        ATTrackingManager.requestTrackingAuthorization { status in
            // Tracking authorization completed. Start loading ads here
        }
        
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // start loading Ads as soon as app become active
        startAutoreloading()
        
        
        if let appOpenPlacement = self.appOpenPlacement, appOpenPlacement.hasAd()  {
            // display loadedFullscreen ad if available
            appOpenPlacement.show()
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // disable placement autoreloading
        stopAutoreloading()
    }
    
    func startAutoreloading() {
        guard let placement = self.appOpenPlacement,
              let viewController = self.window?.rootViewController
        else { return }
        
        // set placement viewController
        AATSDK.controllerViewDidAppear(controller: viewController)
        
        // start placement autoreloading each 60s
        placement.startAutoReload()
    }
    
    func stopAutoreloading() {
        guard let placement = self.appOpenPlacement else { return }
        placement.stopAutoReload()
    }
}

extension AppDelegate: AATReportsDelegate {
    func onReportSent(_ report: String) {
        print("\(#function) -> \(report)")
    }
}

extension AppDelegate: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATKit.AATAdNetwork) {
        print("\(#function)")
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
}
