//
//  LaunchViewController.swift
//  AppOpenSampleApp
//
//  Created by Mohamed Matloub  on 30.06.21.
//

import UIKit
import AATKit

class LaunchViewController: UIViewController {
    @IBOutlet weak var loadingLabel: UILabel!
    
    var timer: Timer?
    var secondsToOpen = 10
    
    var appOpenPlacement: AATAppOpenAdPlacement?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appOpenPlacement =  AATSDK.createAppOpenAdPlacement(placementName: "placement")
        appOpenPlacement?.delegate = self
        
        updateLoadingLabel()
        setupTimer()
    }
    
    func setupTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(fireTimer),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc
    func fireTimer() {
        secondsToOpen -= 1
        updateLoadingLabel()
        
        // check if loading Time is finished to open Home screen
        if secondsToOpen <= 0 {
            openHomeScreen()
        }
    }
    
    func updateLoadingLabel() {
        loadingLabel.text = "Opening app in \(secondsToOpen)s"
    }
    
    func openHomeScreen() {
        let mainViewController = self.storyboard?.instantiateViewController(identifier: "MainViewController")
        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = mainViewController
    }
}

extension LaunchViewController: AATAppOpenPlacementDelegate {
    func aatPauseForAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        // Ad has been closed, and need to open Home Screen
        openHomeScreen()
    }
    
    func aatHaveAd(placement: AATPlacement) {
        // disable timer
        self.timer?.invalidate()
        self.timer = nil
        
        // displaying The App open Ad
        appOpenPlacement?.show()
    }
    
    func aatNoAd(placement: AATPlacement) {
        // disable timer
        self.timer?.invalidate()
        self.timer = nil
        
        openHomeScreen()
    }
}
